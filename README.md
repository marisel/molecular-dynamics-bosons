This code was originally implemented for:

"Out-of-equilibrium simulations of a classical gas with Bose-Einstein statistics"

M. Di Pietro Martı́nez, M. Giuliano, and M. Hoyuelos

Instituto de Investigaciones Fı́sicas de Mar del Plata, 
CONICET, Facultad de Ciencias Exactas y Naturales,
Universidad Nacional de Mar del Plata, Funes 3350, 7600 Mar del Plata, Argentina

https://arxiv.org/abs/2006.06133

The code is freely available under GNU GPL v3.

A Supplemental Material was published together with the paper,
describing the code.

You can find it also in this repository as (soon).